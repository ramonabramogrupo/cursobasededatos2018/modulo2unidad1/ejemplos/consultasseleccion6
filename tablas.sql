create table grupos(
idGrupo int,
nombreGrupo varchar
);

create table productos(
IdProducto int,
NomProducto varchar,
idGrupo int,
Precio float
);

create table vendedores(
idVendedor int,
NombreVendedor varchar,
FechaAlta date,
NIF varchar,
FechaNac date,
direccion varchar,
poblacion varchar,
codPostal varchar,
telefono varchar,
estadoCivil varchar,
guapo bit
);

create table ventas(
codVendedor int,
codProducto int,
fecha date,
kilos int
);